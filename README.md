# PetDesk - Demo web application

## Clone the repository
Run the next command in your git batch if you don't have the repository locally: `git clone https://juanjoseadan@bitbucket.org/juanjoseadan/pdsk-web-app.git`

**Note:** the branch containing the latest changes is _master_

## Steps for running the React web app locally (CLI)

**Note:** before you can start, make sure you have _node package manager (npm)_ installed. If you don't have it, you can follow the instructions described here: https://nodejs.org/ then you can proceed to run the react web app.

**Note 2:** you need to run the API locally before you can start testing.

1. Open a Terminal/Command Line and open the root folder of this project.
2. Install the dependencies for this project by running the next command: `npm install` or `npm i`
3. Once the packages are installed, run the project by running the next command: `npm start`
4. Wait a few seconds, a webpage should open in your default browser. The URL is: `http://localhost:3000/`
5. Have fun!
