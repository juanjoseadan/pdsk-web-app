import React from 'react';

import { 
	Snackbar, IconButton, 
} from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';


interface IPDSKAlertProps {
	open: boolean,
	type: string,
	message: string,
	onClose: () => void,
}

interface IPDSKAlertState {
	open: boolean,
}

class PDSKAlert extends React.Component<IPDSKAlertProps, IPDSKAlertState> {

	constructor(props: IPDSKAlertProps) {
		super(props);

		this.state = {
			open: props.open,
		};
	}

	handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
		if (reason === 'clickaway') {
			return;
		}

		this.setState({
			open: false,
		});

		this.props?.onClose();
	}

	render = () => (
		<Snackbar
			anchorOrigin={{
				vertical: 'bottom',
				horizontal: 'right',
			}}
			open={this.state.open}
			autoHideDuration={10000}
			onClose={this.handleClose}
			message={this.props.message}
			className={'alert-' + this.props.type}
			action={
				<React.Fragment>
					<IconButton size="small" aria-label="close" color="inherit" onClick={this.handleClose}>
						<CloseIcon fontSize="small" />
					</IconButton>
				</React.Fragment>
			}
		/>
	);
}

export default PDSKAlert;