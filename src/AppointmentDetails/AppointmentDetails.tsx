import React from 'react';
import Appointment from '../Models/Appointment';

import moment from 'moment';

import {
	Container, Card, CardContent, Typography, CardActions, Button, Divider, Avatar, ListItem, ListItemAvatar, ListItemText, Grid, Chip,
} from '@material-ui/core';

import TimerIcon from '@material-ui/icons/Timer';
import CheckIcon from '@material-ui/icons/Check';
import PetsIcon from '@material-ui/icons/Pets';
import Moment from 'react-moment';

import PDSKDialog from '../PDSKDialog/PDSKDialog';
import ReScheduleDialog from '../ReScheduleDialog/ReScheduleDialog';

interface IAppointmentDetailsProps {
	appointment: Appointment,
	onAppointmentConfirmed: (appt: Appointment) => void,
	onAppointmentRescheduled: (id: number, dateTime: Date) => void,
}

interface IAppointmentDetailsState {
	dialog: {
		title: string,
		message: string,
		open: boolean,
	},
	reSchedule: {
		open: boolean,
	},
}


class AppointmentDetails extends React.Component<IAppointmentDetailsProps, IAppointmentDetailsState> {
	constructor(props) {
		super(props);

		this.state = {
			dialog: {
				title: '',
				message: '',
				open: false,
			},
			reSchedule: {
				open: false,
			},
		};
	}

	handleConfirmButton = () => {
		const apptDateTime = moment(this.props.appointment.appointmentDateTime);

		this.setState({
			...this.state,
			dialog: {
				title: 'Confirm appointment?',
				message: `Are you sure you want to confirm this Appointment on ${apptDateTime.format('MM/DD/YYYY [at] hh:mm A')}?`,
				open: true,
			},
		});
	}

	handleConfirmed = () => {
		const appointment = this.props.appointment;

		appointment.confirmed = true;

		this.setState({
			...this.state,
			dialog: {
				...this.state.dialog,
				open: false,
			},
		});

		this.props.onAppointmentConfirmed(appointment);
	}

	handleReSchedule = () => {
		this.setState({
			...this.state,
			reSchedule: {
				open: true,
			},
		});
	}
	
	handleReScheduled = (apptDateTime: Date) => {
		console.log('new appointment date and time', moment(apptDateTime).format('YYYY-MM-DD hh:mm'))
		this.setState({
			...this.state,
			reSchedule: {
				open: false,
			},
		});

		this.props.onAppointmentRescheduled(
			this.props.appointment.appointmentId, apptDateTime);
	}

	render = () => (
		<>
			<Container maxWidth="lg" style={{
				marginTop: 16,
			}}>
				<Card>
					<CardContent>
						<Grid
							container
							spacing={1}
							direction="row"
						>
							<Grid item xs={8}>
								<Typography
									variant="h5"
									style={{
										marginBottom: 16,
									}}
								>
									Appointment details
									{
										this.props.appointment.confirmed ?
											<Chip 
												label="Confirmed"
												size="medium"
												color="primary"
												style={{
													marginLeft: 16,
												}}
											/>
										: undefined
									}
								</Typography>
							</Grid>

							<Grid item xs={4} style={{
								textAlign: "end"
							}}>
								<Typography
									component="p"
									variant="caption"
									color="textSecondary"
								>
									<strong>ID: </strong>#
									{ this.props.appointment.appointmentId }
								</Typography>
							</Grid>
						</Grid>

						<Grid
							container
							spacing={1}
							direction="row"
							style={{
								marginBottom: 16
							}}
						>
							{/* Pet */}
							<Grid item xs={6}>
								<ListItem>
									<ListItemAvatar>
										<Avatar style={{
											width: 32,
											height: 32,
										}}>
											<PetsIcon />
										</Avatar>
									</ListItemAvatar>

									<ListItemText
										primary={
											<React.Fragment>
												<Typography
													variant="subtitle2"
												>
													<strong>Pet: </strong> {this.props.appointment.petName}
												</Typography>
											</React.Fragment>
										}
										secondary={
											<React.Fragment>
												<Typography
													component="p"
													variant="caption"
													color="textSecondary"
												>
													<strong>Species: </strong>
													{
														this.props.appointment.petSpecies ?
															this.props.appointment.petSpecies : 'Unknown'
													}
													{
														this.props.appointment.petBreed ?
															<>
																&nbsp;({this.props.appointment.petBreed})
															</>
														: undefined
													}
												</Typography>
											</React.Fragment>
										}
									/>
								</ListItem>
							</Grid>

							{/* Owner */}
							<Grid item xs={6}>
								<ListItem>
									<ListItemAvatar>
										<Avatar
											src={'https://i.pravatar.cc/36?ts=' + new Date()}
											style={{
												width: 32,
												height: 32,
											}}
										/>
									</ListItemAvatar>

									<ListItemText
										primary={
											<React.Fragment>
												<Typography
													variant="subtitle2"
												>
													<strong>Owner: </strong> {this.props.appointment.ownerFullName}
												</Typography>
											</React.Fragment>
										}
										secondary={
											<React.Fragment>
												<Typography
													component="p"
													variant="caption"
													color="textSecondary"
												>
													<strong>ID: </strong> #
													{
														this.props.appointment.ownerId ?
															this.props.appointment.ownerId : ''
													}
												</Typography>
											</React.Fragment>
										}
									/>
								</ListItem>
							</Grid>
						</Grid>

						<Typography
							component="span"
							variant="subtitle1"
							color="textPrimary"
						>
							<strong>Treatment / Service: </strong>
							<br />
							{ this.props.appointment.appointmentType }
						</Typography>

						<br />
						<br />

						<Typography
							component="span"
							variant="subtitle1"
							color="textPrimary"
						>
							<strong>Appointment date and time: </strong>
							<br />
							
							<Moment 
								date={this.props.appointment.appointmentDateTime}
								format="MM/DD/YYYY [at] hh:mm A"
							/>
						</Typography>

						<br />
						<br />

						<Typography
							component="p"
							variant="caption"
							color="textPrimary"
							style={{
								marginBottom: 8,
							}}
						>
							<strong>Description:</strong>
							<br />
							Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vel quod deserunt omnis assumenda consequatur, modi perspiciatis asperiores recusandae doloribus ut necessitatibus quae aliquid eius hic. Dolores iure amet repellendus repellat.
						</Typography>

						<Divider />
					</CardContent>

					<CardActions style={{
						alignItems: 'flex-end',
						justifyContent: 'flex-end',
					}}>
						<Button
							size="medium"
							color="secondary"
							variant="outlined"
							onClick={this.handleReSchedule}
							startIcon={
								<React.Fragment>
									<TimerIcon />
								</React.Fragment>
							}
						>
							Re-schedule
						</Button>

						<Button
							size="medium"
							color="primary"
							variant="outlined"
							onClick={this.handleConfirmButton}
							disabled={this.props.appointment.confirmed}
							startIcon={
								<React.Fragment>
									<CheckIcon />
								</React.Fragment>
							}
						>
							Confirm
						</Button>
					</CardActions>
				</Card>
			</Container>

			{
				this.state.reSchedule.open ?
					<ReScheduleDialog
						currentDateTime={this.props.appointment.appointmentDateTime}
						open={this.state.reSchedule.open}
						onAcceptButtonClick={this.handleReScheduled}
						onCancelButtonClick={() => {
							this.setState({
								...this.state,
								reSchedule: {
									open: false,
								},
							});
						}}
					/>
				: undefined
			}

			<PDSKDialog
				{...this.state.dialog}
				acceptButtonText="ACCEPT"
				onAcceptButtonClick={this.handleConfirmed}
				cancelButtonText="CANCEL"
				onCancelButtonClick={() => {
					this.setState({
						...this.state,
						dialog: {
							...this.state.dialog,
							open: false,
						},
					});
				}}
			/>
		</>
	);
}

export default AppointmentDetails;