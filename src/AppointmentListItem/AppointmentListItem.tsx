import React from 'react';
import Moment from 'react-moment';
import Appointment from '../Models/Appointment';

import { 
	ListItem, ListItemText, ListItemAvatar, Avatar, Typography, Chip, 
} from '@material-ui/core';

import EventIcon from '@material-ui/icons/Event';

interface IAppointmentListItemProps {
	appointment: Appointment,
	selectedAppointmentId: number | undefined,
	onSelectedItem: (id: number) => void,
}

class AppointmentListItem extends React.Component<IAppointmentListItemProps> {
	render = () => (
		<ListItem
			button
			selected={this.props.selectedAppointmentId === this.props.appointment.appointmentId}
			onClick={() => this.props.onSelectedItem(this.props.appointment.appointmentId)}
		>
			<ListItemAvatar>
				<Avatar>
					<EventIcon />
				</Avatar>
			</ListItemAvatar>

			<ListItemText
				primary={
					<React.Fragment>
						<Typography
							variant="inherit"
							color="textPrimary"
						>
							{ this.props.appointment.appointmentType }
							{
								this.props.appointment.confirmed ?
									<Chip 
										label="Confirmed"
										size="small"
										color="primary"
										style={{
											marginLeft: 16,
										}}
									/>
								: undefined
							}
						</Typography>
					</React.Fragment>
				}
				secondary={
					<React.Fragment>
						<Typography
							component="span"
							variant="body2"
							color="textPrimary"
						>
							<strong>Owner: </strong> { `${this.props.appointment.ownerFullName}` ?? 'Unknown' }
						</Typography>

						<br />

						<Typography
							component="span"
							variant="body2"
							color="textSecondary"
						>
							<strong>Pet: </strong> { `${this.props.appointment.petName}` ?? 'Unknown' }
						</Typography>


						<Typography
							component="span"
							variant="caption"
							color="textSecondary"
						>
							<br />
							<Moment 
								date={this.props.appointment.appointmentDateTime}
								format="MM/DD/YYYY [at] hh:mm A"
							/>
						</Typography>
					</React.Fragment>
							}
			/>
		</ListItem>
	);
}

export default AppointmentListItem;