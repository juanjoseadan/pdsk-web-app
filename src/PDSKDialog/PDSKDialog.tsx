import React from 'react';

import { 
	Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button, 
} from '@material-ui/core';

interface IPDSKDialogProps {
	title: string,
	message: string,
	acceptButtonText: string,
	onAcceptButtonClick: () => void,
	cancelButtonText?: string,
	onCancelButtonClick?: () => void,
	open: boolean,
}

interface IPDSKDialogState {
	open: boolean,
}

class PDSKDialog extends React.Component<IPDSKDialogProps, IPDSKDialogState> {
	constructor(props) {
		super(props);

		this.state = {
			open: this.props.open,
		};
	}

	render = () => (
		<Dialog
			open={this.props.open}
		>
			<DialogTitle>
				{ this.props.title }
			</DialogTitle>

			<DialogContent>
				<DialogContentText>
					{ this.props.message }
				</DialogContentText>
			</DialogContent>

			<DialogActions>
				{
					(this.props.cancelButtonText && this.props.onCancelButtonClick) ?
						<Button 
							size="medium"
							variant="text"
							onClick={this.props.onCancelButtonClick}
						>
							{ this.props.cancelButtonText }
						</Button>
					: undefined
				}

				<Button
					size="medium"
					variant="text"
					color="primary"
					onClick={this.props.onAcceptButtonClick}
				>
					{ this.props.acceptButtonText }
				</Button>
			</DialogActions>
		</Dialog>
	);
}

export default PDSKDialog;