import React from 'react';
import * as _ from 'lodash';

import HttpClient from './Services/HttpClient';

import { 
	Toolbar, AppBar, Typography, Grid, Backdrop, CircularProgress,
} from '@material-ui/core';

import AppointmentsList from './AppointmentsList/AppointmentsList';
import AppointmentDetails from './AppointmentDetails/AppointmentDetails';
import PDSKAlert from './PDSKAlert/PDSKAlert';

import Appointment from './Models/Appointment';

interface PDSKState {
	appointments: Appointment[],
	selectedAppointment?: Appointment,
	alert: {
		shown: boolean,
		type: string,
		message: string,
	},
	loading: boolean,
}

class PDSKApp extends React.Component<{}, PDSKState> {

	constructor(props) {
		super(props);

		this.state = {
			appointments: [],
			alert: {
				shown: false,
				type: '',
				message: '',
			},
			loading: false,
		};
	}

	componentDidMount = () => {
		this.getAppointments();
	}

	getAppointments = async () => {
		this.setState({
			...this.state,
			loading: true,
		});

		try {
			const result: Appointment[] = await HttpClient.get('api/appointments');
			const defaultAppointment = (result && result.length > 0) ? result[0] : undefined;
			
			this.setState({
				...this.state,
				appointments: result,
				selectedAppointment: defaultAppointment,
			});
		} catch (error) {
			console.error(error);

			this.setState({
				...this.state,
				alert: {
					shown: true,
					type: 'error',
					message: 'Something went wrong. Please try again.',
				},
			});
		} finally {
			this.setState({
				...this.state,
				loading: false,
			});
		}
	}

	handleAlertClose = () => this.state = {
		...this.state,
		alert: {
			...this.state.alert,
			shown: false,
		},
	}

	handleOnAppointmentSelected = (appt: Appointment | undefined) => {
		this.setState({
			...this.state,
			selectedAppointment: appt,
		});
	}

	handleAppointmentConfirmed = (appt: Appointment) => {
		const appointments = _.cloneDeep(this.state.appointments);
		const index = appointments.findIndex(x => x.appointmentId === appt.appointmentId);

		if (index >= 0) {
			console.log('will update state...')
			appointments[index] = appt;
			appointments[index].confirmed = true;

			this.setState({
				...this.state,
				appointments: appointments,
			});
			console.log('State was updated')
		}
	}

	handleAppointmentRescheduled = (id: number, dateTime: Date) => {
		const appointments = this.state.appointments;
		const index = appointments.findIndex(x => x.appointmentId === id);

		if (index >= 0) {
			appointments[index].appointmentDateTime = dateTime;

			this.setState({
				...this.state,
				appointments: appointments,
				alert: {
					shown: true,
					type: 'success',
					message: `Appointment #${id} successfully Re-Scheduled`,
				},
			});
		}
	}

	render = () => (
		<div className="App">
			<AppBar position="sticky">
				<Toolbar>
					<Typography variant="h6">
						My Appointments
					</Typography>
				</Toolbar>
			</AppBar>

			<Backdrop 
				open={this.state.loading} 
				onClick={() => this.setState({
					...this.state,
					loading: false,
				})}
			>
				<CircularProgress color="secondary" />
			</Backdrop>

			<Grid 
				container
				spacing={1}
				direction="row"
			>
				<Grid item xs={4} className="appointments-column">
					<AppointmentsList
						appointments={this.state.appointments}
						selectedAppointment={this.state.selectedAppointment}
						onAppointmentSelected={this.handleOnAppointmentSelected}
					/>
				</Grid>

				<Grid item xs={8}>
					{
						this.state.selectedAppointment ?
							<AppointmentDetails
								appointment={this.state.selectedAppointment}
								onAppointmentConfirmed={this.handleAppointmentConfirmed}
								onAppointmentRescheduled={this.handleAppointmentRescheduled}
							/>
						: undefined
					}
				</Grid>
			</Grid>

			<PDSKAlert
				open={this.state.alert?.shown}
				message={this.state.alert?.message}
				type={this.state.alert?.type}
				onClose={this.handleAlertClose}
			/>
		</div>
	);
}

export default PDSKApp;