import axios from 'axios';

const HttpClient = axios.create({
	baseURL: 'http://localhost:5001',
	timeout: 80000,
	headers: {
		'Content-Type': 'application/json',
	},
});

HttpClient.interceptors.response
	.use(
		(response) => response.data,
		(error) => {
			console.log('error jj', error)
			if (error.response.status === 401) {
				return Promise.reject(error.response);
			}
			if (error.response) {
				return Promise.reject(error.response.data);
			}

			return Promise.reject('[HttpClient] - Network Error');
		},
	);

export default HttpClient;
