import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import PDSKApp from './PDSKApp';

import { ThemeProvider } from '@material-ui/core';

import { createMuiTheme } from '@material-ui/core/styles';
import lightBlue from '@material-ui/core/colors/lightBlue';
import purple from '@material-ui/core/colors/purple';

const theme = createMuiTheme({
	palette: {
		primary: {
			main: lightBlue[800],
		},
		secondary: {
			main: purple[200],
		},
	},
});

ReactDOM.render(
	<React.StrictMode>
		<ThemeProvider theme={theme}>
			<PDSKApp />
		</ThemeProvider>
	</React.StrictMode>,
	document.getElementById('root')
);