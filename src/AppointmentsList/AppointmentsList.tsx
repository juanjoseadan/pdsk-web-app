import React from 'react';

import { List } from '@material-ui/core';

import AppointmentListItem from '../AppointmentListItem/AppointmentListItem';
import Appointment from '../Models/Appointment';

interface IAppointmentsListProps {
	appointments: Appointment[],
	selectedAppointment: Appointment | undefined,
	onAppointmentSelected: (appt: Appointment | undefined) => void,
}

class AppointmentsList extends React.Component<IAppointmentsListProps> {

	handleSelectedItem = (id: number) => {
		this.props.onAppointmentSelected(
			this.props.appointments.find(a => a.appointmentId === id));
	}

	render = () => (
		<List>
			{
				this.props.appointments?.length > 0 ? 
					this.props.appointments.map(a => 
						<AppointmentListItem
							key={a.appointmentId}
							appointment={a}
							selectedAppointmentId={this.props.selectedAppointment?.appointmentId}
							onSelectedItem={this.handleSelectedItem}
						/>
					)
					: undefined
			}
		</List>
	);
}

export default AppointmentsList;