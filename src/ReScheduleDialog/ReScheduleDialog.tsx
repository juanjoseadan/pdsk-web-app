import React from 'react';
import moment from 'moment';

import { 
	Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button, Grid, TextField, 
} from '@material-ui/core';

interface IReScheduleProps {
	currentDateTime: Date,
	open: boolean,
	onCancelButtonClick: () => void,
	onAcceptButtonClick: (apptDateTime: Date) => void,
}

interface IReScheduleState {
	date: string,
	time: string,

	dateTime: moment.Moment,
}

class ReScheduleDialog extends React.Component<IReScheduleProps, IReScheduleState> {

	constructor(props) {
		super(props);

		// Set default re-schedule date and time to next week
		const nextWeek = moment(this.props.currentDateTime).add(7, 'days');

		this.state = {
			date: nextWeek.format('YYYY-MM-DD'),
			time: nextWeek.format('hh:mm'),
			dateTime: nextWeek,
		};
	}

	handleReSchedule = () => {
		const dateTime = this.state.dateTime.local(true).toDate();
		this.props.onAcceptButtonClick(dateTime);
	}

	getDate = () => {
		const nextWeek = moment(this.props.currentDateTime).add(7, 'days');

		this.setState({
			...this.state,
			date: nextWeek.format('YYYY-MM-DD'),
		});

		return nextWeek.format('YYYY-MM-DD');
	}

	getTime = () => {
		const nextWeek = moment(this.props.currentDateTime).add(7, 'days');

		this.setState({
			...this.state,
			time: nextWeek.format('hh:mm'),
		});

		return nextWeek.format('hh:mm');
	}

	render = () => (
		<Dialog
			open={this.props.open}
		>
			<DialogTitle>Re-Schedule Appointment</DialogTitle>

			<DialogContent>
				<DialogContentText>
					Select a date and time to Re-Schedule this Appointment
				</DialogContentText>

				<Grid container spacing={1}>
					<Grid item xs={6}>
						<TextField
							label="Date"
							type="date"
							value={this.state.date}
							onChange={evt => {
								const newDateTime = moment(`${evt.target.value} ${this.state.dateTime.format('hh:mm')}`);

								this.setState({
									...this.state,
									date: evt.target.value,
									dateTime: newDateTime,
								});
							}}
							InputLabelProps={{
								shrink: true,
							}}
							style={{
								width: '100%',
							}}
						/>
					</Grid>

					<Grid item xs={6}>
						<TextField
							label="Time"
							type="time"
							value={this.state.time}
							onChange={evt => {
								const newDateTime = moment(`${this.state.dateTime.format('YYYY-MM-DD')} ${evt.target.value}`);

								this.setState({
									...this.state,
									time: evt.target.value,
									dateTime: newDateTime,
								});
							}}
							InputLabelProps={{
								shrink: true,
							}}
							style={{
								width: '100%',
							}}
						/>
					</Grid>
				</Grid>
			</DialogContent>

			<DialogActions>
				<Button
					size="medium"
					variant="text"
					onClick={this.props.onCancelButtonClick}
				>
					Cancel
				</Button>

				<Button
					size="medium"
					variant="text"
					color="primary"
					onClick={this.handleReSchedule}
				>
					Accept
				</Button>
			</DialogActions>
		</Dialog>
	);
}

export default ReScheduleDialog;