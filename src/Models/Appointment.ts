interface Appointment {
	appointmentId: number,
	ownerFullName: string,
	petName: string,
	petSpecies: string,
	petBreed: string,
	appointmentType: string,
	appointmentDateTime: Date,
	ownerId: number,
	confirmed: boolean,
}

export default Appointment;