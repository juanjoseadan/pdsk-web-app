interface User {
	userId: number,
	firstName: string,
	lastName: string,
	vetDataId: string,
}

export default User;