interface Animal {
	animalId: number,
	firstName: string,
	species: string,
	breed: string,
}

export default Animal;